'use strict';

const db_video = require('./video/video_servers');

function createResponse(statusCode, message) {
  return {
    statusCode: statusCode,
    body: JSON.stringify(message)
  };
}

module.exports.status = (event, context, callback) => {
  const response = JSON.stringify({
      message: 'Server working successfully!!!'
    }
  );
  callback(null, createResponse(200, response));
};

module.exports.getVideos = (event, context, callback) => {
  db_video.getVideos().then(response => {
    callback(null, createResponse(200, response));
  }).catch((error) => callback(null, createResponse(200, error)));
};