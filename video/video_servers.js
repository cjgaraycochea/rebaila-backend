'use strict';

const AWS = require('aws-sdk');
let dynamo = new AWS.DynamoDB.DocumentClient();

const TABLE_NAME = 'rebaila_videos';

module.exports.initializateDynamoClient = newDynamo => {
  dynamo = newDynamo;
};

module.exports.getVideos = () => {
  const params = {
    TableName: TABLE_NAME
  };

  return dynamo.scan(params).promise()
    .then((response) => {
      return response;
    })
    .catch((error)=> { 
      return error
    });
}