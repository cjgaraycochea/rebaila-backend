# SERVERLESS - AWS - GITLAB CI/CD

## SERVERLESS CONFIGURATION
Basic structure for backend installation. The Serverless create the instruccion for create automatically the environment in Amazon Web Services. Create the API endpoints (Lambda and API Gateway), database (Dynamo DB).

### Replace line "base-backend" by an API name "project-name-backend"
Edit the line "services:" in serverless.yml and package.json
>service: base-backend

### Install Serverless
> sudo npm install -g serverless

### Stage to "dev" and region "eu-central-1"
Edit the lines in serverles.yml.
> stage: dev
> region: eu-central-1

### Create Role with all permissions in AIM of AWS
Create a User Role in AIM with Policy "AdministratorAccess" and edit the lines in serverles.yml
> profile: <AWS_USER_ROLE_IAM>

### Add User Permissions with IAM of Amazon Web Services
> sls config credentials --provider aws --key <AWS_ACCESS_KEY_ID> --secret <AWS_SECRET_ACCESS_KEY> --profile <AWS_USER_ROLE_IAM>

### Check the endpoint
> sls invoke local -f getVideos

### Check if Serverless create successfully the API Gateway and Lambda.
> npm run deploy

# REMOVE SERVERLESS PROCESS IN AWS FULL CONFIGURATION
> sls remove -s dev -r eu-central-1

# GITLAB CI-CD
Finished the Serverless configuration you can create the Gitlab CI/CD (Continuous Integration and Continuous Delivery). 
> AWS_ACCESS_KEY_ID: <AWS_ACCESS_KEY_ID> // variable - Protect
> AWS_SECRET_ACCESS_KEY: <AWS_SECRET_ACCESS_KEY> // variable - Protect
> AWS_USER_ROLE_IAM: <AWS_USER_ROLE_IAM>

## Create Gitlab Repository.
Create blank project in http://gitlab.com follow the instruction for git setup.
> git init --initial-branch=main
> git remote add origin git@gitlab.com:cjgaraycochea/base-backend.git
> git add .
> git commit -m "Initial commit"
> git push -u origin main

## Set the environment variables for AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, others
> https://gitlab.com/cjgaraycochea/base-backend/-/settings/ci_cd

## Resources
Serverless configuration example:
> https://www.youtube.com/watch?v=SjcbxIldTkI

Gitlab CI/CD configuration example:
> https://www.youtube.com/watch?v=C9ZhZE0kusw

DynamoDB and Serverless
https://www.youtube.com/watch?v=EzgyTzJll5U